
variable "main_domain_name" {
  type = string
  default = "oscarvillarraga.com"
}

variable "main_domain_zone_id" {
  type = string
  default= "Z0668419237BRC9MEQE4B"
}
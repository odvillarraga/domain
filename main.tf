terraform {
  backend "s3" {
    bucket = "odv-domain"
    region = "us-east-1"
    key = "domain"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_route53_zone" "internal" {
  name = "internal.${var.main_domain_name}"

  tags = {
    Environment = "dev"
  }
}

resource "aws_route53_record" "internal_ns" {
  zone_id = var.main_domain_zone_id
  name    = "internal.${var.main_domain_name}"
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.internal.name_servers
}